/* Priyanka Bhasme
**Native Practice myBicycle v1.0
** using fixed/world co-ordanate
** bad practice
*/

// Header files
#include<Windows.h>
#include<gl\GL.h>
#include<gl\GLU.h>
#include<math.h>

// Pragma
#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")



// macros declaration 
#define WINWIDTH 800
#define WINHEIGHT 600
// for calculating anlges
#define PI 3.1415926535898

float gwSideofTriangle = 2.0f, angleWheels;
bool gbFullScreen, gbEscapeKeyIsPressed, gbActiveWindow = false;
bool gbDone;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;
// ask sir - how error goes when curly brackets are added????????????
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
float yAngle;


// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);



// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIndtance, LPSTR lpszCmdLine, int nCmdShow)
{

	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//	variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	// make window color blue
	wndclass.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("My ride"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				gbDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				update();
				display();
				if (gbEscapeKeyIsPressed == true)
					gbDone = true;
			}
		}
	}

	uninitialize();
	return((int)msg.wParam);

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	// code
	//function prototype
	//void display(void);

	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		/*case WM_PAINT:
		display();
		break;
		case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				gwSideofTriangle = 2.0;
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		gwSideofTriangle = WINWIDTH;
		ShowCursor(TRUE);
	}
}



void initialize(void) {
	//function prototypes
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	//Initialization of pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (wglMakeCurrent(ghdc, ghrc) == false) {

		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// first OpenGL call
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(WINWIDTH, WINHEIGHT);

}
void update(void) {

	angleWheels += 0.1f;
	if (angleWheels >= 360) {
		angleWheels = 0;
	}


	yAngle += 0.1f;
	if (yAngle >= 360) {
		yAngle = 0.1f;
	}
}


void display(void) {

	//void drawRectangle(float rectangleScale);
	void drawRectangle(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float Dx, float Dy);
	void drawGraphPaper(void);
	//void drawTriangle(float traiangleScale);
	void drawTriangle(float Ax, float Ay, float Bx, float By, float Cx, float Cy);
	void drawMulticoloredTriangle(float traiangleScale);
	void drawCircle(float circleScale, float shiftDist, float startPoint);
	void drawLines(float Ax, float Ay, float Bx, float By);
	void drawARectangle(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float Dx, float Dy);
	void drawBRectangle(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float Dx, float Dy);
	void drawACircle(float circleScale, float shiftDist, float startPoint);

	float wheelsSize = 0.2f;
	float fStartingPoint = 0.75f;
	float centerXRightCircle;
	float centerYRightCircle;
	float distanceBetweenWheels = 0.6f;
	float centerXLeftCircle;
	float centerYLeftCircle;
	float backSeatTriangleAx;
	float backSeatTriangleAy;
	float backSeatTriangleBx;
	float backSeatTriangleBy;
	float backSeatTriangleCx;
	float backSeatTriangleCy;
	float frontTriangleDx;
	float frontTriangleDy;
	float frontTriangleBx;
	float frontTriangleBy;
	float frontTriangleCx;
	float frontTriangleCy;
	float seatStartX;
	float seatStartY;
	float handleTopX;
	float handleTopY;
	float handleBottomX;
	float handleBottomY;
	float cValue;
	float angle;
	float mValue;

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, -0.0f, -2.0f);
	// green road
	drawBRectangle(-1.0f, -0.2f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -0.2f);
	drawGraphPaper();
	//drawCircle(wheelsSize, 0.0f, fStartingPoint);
	//drawCircle(wheelsSize, distanceBetweenWheels, fStartingPoint);
	//glRotatef(yAngle, 0.0f, 1.0f, 0.0);
	centerXRightCircle = fStartingPoint;
	centerYRightCircle = 0.0f;
	centerXLeftCircle = fStartingPoint - distanceBetweenWheels;
	centerYLeftCircle = centerYRightCircle;

	backSeatTriangleAx = fStartingPoint;
	backSeatTriangleAy = 0;
	backSeatTriangleBx = (fStartingPoint - (distanceBetweenWheels / 2));
	backSeatTriangleBy = 0 - 0.03;
	backSeatTriangleCx = backSeatTriangleBx + ((backSeatTriangleAx - backSeatTriangleBx) / 2);
	backSeatTriangleCy = 0.35f;

	drawTriangle(backSeatTriangleAx, backSeatTriangleAy, backSeatTriangleBx, backSeatTriangleBy, backSeatTriangleCx, backSeatTriangleCy);
	seatStartX = backSeatTriangleCx;
	seatStartY = backSeatTriangleCy;

	drawRectangle(seatStartX, seatStartY, seatStartX + 0.15, seatStartY,
		seatStartX + 0.15, seatStartY + 0.05, seatStartX + 0.02, seatStartY);

	frontTriangleBx = backSeatTriangleBx;
	frontTriangleBy = backSeatTriangleBy;
	frontTriangleCx = backSeatTriangleCx;
	frontTriangleCy = backSeatTriangleCy;
	frontTriangleDx = frontTriangleCx - (distanceBetweenWheels / 2);
	frontTriangleDy = frontTriangleCy;

	drawTriangle(frontTriangleBx, frontTriangleBy, frontTriangleCx, frontTriangleCy, frontTriangleDx, frontTriangleDy);
	drawLines(centerXLeftCircle, centerYLeftCircle, frontTriangleDx, frontTriangleDy);

	handleBottomX = frontTriangleDx;
	handleBottomY = frontTriangleDy;


	handleTopX = handleBottomX + 0.04f;
	mValue = ((frontTriangleDy - centerYLeftCircle) / (frontTriangleDx - centerXLeftCircle));
	//angle = 
	//angle = atan((frontTriangleDy/ frontTriangleDx));
	//cValue = handleTopX * tan(angle);
	handleTopY = (handleTopX)* (frontTriangleDy / frontTriangleDx - centerXLeftCircle) + 0.08;//* mValue + cValue;
	drawLines(handleBottomX, handleBottomY, handleTopX, handleTopY);
	drawLines(handleTopX, handleTopY, handleTopX + 0.1f, handleTopY);
	drawARectangle(-1.0f, -1.0f, 1.0f, -1.0f, 1.0f, -0.205f, -1.0f, -0.205f);
	//drawACircle(0.05f, 0.0f, 0.75f);

	//glRotatef(angleWheels, 0.0f, 1.0f, 0.0f);
	// I cannot rotate the wheels since I have drawn whole cycle in earth co-ordinate system
	// not using glTranslatef(). Because of that whenever I rotate anything is rotates across
	// earth x/y/z axis. I must shift the co-ordinate and load identity everytime I draw new shape
	// for Bicycle.
	// I am writing another version of this demo using, glTranslate()
	drawCircle(wheelsSize, 0.0f, fStartingPoint);
	drawCircle(wheelsSize, distanceBetweenWheels, fStartingPoint);

	SwapBuffers(ghdc);

}

// draws Rectngle
void drawARectangle(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float Dx, float Dy) {

	glBegin(GL_QUADS);
	glColor3f(0.1f, 0.1f, 0.1f);
	//glColor3f(105.0/255.0, 105.0 / 255.0, 105.0 / 255.0);
	glVertex3f(Ax, Ay, 0.0f);
	glColor3f(0.5f, 0.5f, 0.5f);
	glVertex3f(Bx, By, 0.0f);
	glColor3f(0.1f, 0.1f, 0.1f);
	glVertex3f(Cx, Cy, 0.0f);
	glColor3f(0.7f, 0.7f, 0.7f);
	glVertex3f(Dx, Dy, 0.0f);
	glEnd();

}

// draws Rectngle
void drawBRectangle(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float Dx, float Dy) {

	glBegin(GL_QUADS);
	//glColor3f(40.0/255.0f, 0.0f, 40.0 / 255.0f);
	glColor3f(0.0f, 0.0f, 0.0f);
	glVertex3f(Ax, Ay, 0.0f);
	//glColor3f(50.0/255.0f, 0.0f, 50.0/255.0f);
	//glColor3f(75.0 / 255.0f, 61.0/255.0f, 139.0 / 255.0f);
	//glColor3f(0.0f,0.0f,0.0f);
	glVertex3f(Bx, By, 0.0f);
	//glColor3f(25.0 / 255.0f, 25.0/255.0f, 25.0 / 255.0f);
	glVertex3f(Cx, Cy, 0.0f);
	//glColor3f(20.0 / 255.0f, 20.0/255.0f, 60.0 / 255.0f);
	glVertex3f(Dx, Dy, 0.0f);
	glEnd();

}

// draws Rectngle
void drawRectangle(float Ax, float Ay, float Bx, float By, float Cx, float Cy, float Dx, float Dy) {

	glBegin(GL_QUADS);
	glColor3f(0.5f, 1.0f, 1.0f);
	//glColor3f(105.0/255.0, 105.0 / 255.0, 105.0 / 255.0);
	glVertex3f(Ax, Ay, 0.0f);
	glVertex3f(Bx, By, 0.0f);
	glVertex3f(Cx, Cy, 0.0f);
	glVertex3f(Dx, Dy, 0.0f);
	glEnd();

}

// draws Rectngle
void drawLines(float Ax, float Ay, float Bx, float By) {

	glBegin(GL_LINES);
	glColor3f(0.5f, 1.0f, 1.0f);
	glVertex3f(Ax, Ay, 0.0f);
	glVertex3f(Bx, By, 0.0f);
	glEnd();

}

// draws triangle
void drawMulticoloredTriangle(float traiangleScale) {

	glBegin(GL_TRIANGLES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, traiangleScale, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-traiangleScale, -traiangleScale, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(traiangleScale, -traiangleScale, 0.0f);
	glEnd();

}

// draws circle
void drawACircle(float circleScale, float shiftDist, float startPoint) {//, gstColorCodes * ptrColorCode) {

	float angle = 0.0f;
	glPointSize(2.0f);
	glBegin(GL_POINTS);
	//glColor3f(ptrColorCode->rValue, ptrColorCode->gValue, ptrColorCode->bValue
	glColor3f(1.0f, 1.0f, 1.0f);
	for (int i = 0; i < 1000; i++) {

		angle = 2 * PI * i / 1000.0f;
		glVertex2f((cos(angle) * circleScale + (-startPoint - 0.1f)), (sin(angle) * circleScale) + startPoint + 0.1);

	}

	glEnd();

}


// draws circle
void drawCircle(float circleScale, float shiftDist, float startPoint) {//, gstColorCodes * ptrColorCode) {

	float angle = 0.0f;
	glPointSize(4.0f);
	glBegin(GL_POINTS);
	//glColor3f(ptrColorCode->rValue, ptrColorCode->gValue, ptrColorCode->bValue
	glColor3f(1.0f, 1.0f, 1.0f);
	for (int i = 0; i < 1000; i++) {

		angle = 2 * PI * i / 1000.0f;
		glVertex2f((cos(angle) * circleScale) + (startPoint - shiftDist), (sin(angle) * circleScale));

	}

	glEnd();

}

// draws triangle
void drawTriangle(float Ax, float Ay, float Bx, float By, float Cx, float Cy) {

	glLineWidth(5.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(1.0f, 1.0f, 1.0f);
	glVertex3f(Ax, Ay, 0.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(Bx, By, 0.0f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(Cx, Cy, 0.0f);
	glEnd();

}

// draw graph paper

void drawGraphPaper(void) {

	float xIndex = 0.0f;
	float yIndex = 0.0f;
	glLineWidth(10.0f);
	//start drawing graph
	glBegin(GL_LINES);
	glColor3f(0.25f, 0.25f, 0.25f);
	glVertex3f(-1.0f, -0.205f, 0.0f);
	glVertex3f(1.0f, -0.205f, 0.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	//glVertex3f(0.0f, 1.0f, 0.0f);
	//glVertex3f(0.0f, -1.0f, 0.0f);
	glEnd();
	/*
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (xIndex = -0.95f; xIndex < 1.0f; (xIndex += 0.05f))
	{
	glVertex3f(xIndex, 1.0f, 0.0f);
	glVertex3f(xIndex, -1.0f, 0.0f);
	}


	for (yIndex = -0.95f; yIndex < 1.0f; (yIndex += 0.05f))
	{
	glVertex3f(1.0f, yIndex, 0.0f);
	glVertex3f(-1.0f, yIndex, 0.0f);
	}
	glEnd();
	// end of graph
	*/

}



void resize(int width, int height) {

	float adjustedB;
	float adjustedT;
	float adjustedL;
	float adjustedR;
	float b = -1.0f;
	float t = 1.0f;
	float l = -1.0f;
	float r = 1.0f;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION); // change matrix mode to projection
	glLoadIdentity();
	//glFrustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 10.0f);
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	/*if (height == 0) {
	height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);*/

	/*if (height == 0) {
	height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION); // change matrix mode to projection
	glLoadIdentity();

	if (width <= height) {
	adjustedB = b * ((GLfloat)width / (GLfloat)height);
	adjustedT = t * ((GLfloat)width / (GLfloat)height);
	glFrustum(-1.0f, 1.0f, adjustedB, adjustedT, 1.0f, 10.0f);
	//glViewport(0, 0, adjustedB, adjustedT);
	}
	else
	{
	adjustedB = l * ((GLfloat)height / (GLfloat)width);
	adjustedT = r * ((GLfloat)height / (GLfloat)width);
	glFrustum(-1.0f, 1.0f, adjustedB, adjustedT, 1.0f, 10.0f);
	//glViewport(0, 0, adjustedB, adjustedT);
	}*/
}

void uninitialize(void) {

	if (gbFullScreen == true) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER |
			SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	DestroyWindow(ghwnd);
}