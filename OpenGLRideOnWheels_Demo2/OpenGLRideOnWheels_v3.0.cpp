/* Priyanka Bhasme
**Native Ride on wheels v2.0
**Output - animated bicycle
*/

// Header files
#include<Windows.h>
#include<gl\GL.h>
#include<gl\GLU.h>
#include<math.h>

// Pragma
#pragma comment(lib, "opengl32.lib")
// we need this to link
#pragma comment(lib, "glu32.lib")


// macros declaration 
#define WINWIDTH 800
#define WINHEIGHT 800
// for calculating anlges
#define PI 3.1415926535898


float angleWheels, angleRect, angleDetla, xMove = 3.0f;
int rotationsCount = 10000;
float gwSideofTriangle = 2.0f;
bool gbFullScreen, gbEscapeKeyIsPressed, gbActiveWindow = false;
bool gbDone;
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;
DWORD dwStyle;

// for Bicycle
float gfWheelSize = 0.2f;
float gfStartingPoint = 0.75f;
float gfDistanceBetweenWheels = 1.5f;
float yPara= 0.0, xPara = 0.0;



// ask sir - how error goes when curly brackets are added????????????
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };



// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevIndtance, LPSTR lpszCmdLine, int nCmdShow)
{

	//function prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);

	//	variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.cbClsExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	// make window color blue
	wndclass.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("Ride on wheels"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		0,
		0,
		WINWIDTH,
		WINHEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (gbDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				gbDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{

				if (gbEscapeKeyIsPressed == true)
					gbDone = true;
				update();
				display();

			}
		}
	}

	uninitialize();
	return((int)msg.wParam);

}


LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam) {

	// code
	//function prototype
	//void display(void);

	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	//code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
		/*case WM_PAINT:
		display();
		break;
		case WM_ERASEBKGND:
		return(0);*/
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;
		case 0x46: //for 'f' or 'F'
			if (gbFullScreen == false)
			{
				ToggleFullscreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	//variable declarations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
				gwSideofTriangle = 2.0;
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		gwSideofTriangle = WINWIDTH;
		ShowCursor(TRUE);
	}
}

void initialize(void) {
	//function prototypes
	void resize(int, int);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
	//Initialization of pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (iPixelFormatIndex == 0) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);

	if (ghrc == NULL) {

		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;

	}

	if (wglMakeCurrent(ghdc, ghrc) == false) {

		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// first OpenGL call
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	resize(WINWIDTH, WINHEIGHT);
	// when I uncomment this and run the code, triagle is clipped at the vertices.
	// why this happens?? after toggleing the fullscreen, it looks fine.

}

void display(void) {

	void drawTriangle(float traiangleScale);
	void drawGraphPaper(void);
	void drawRectangle(float rectScale);
	void drawCircle(float circleScale);
	void drawHandle(void);
	void drawMountains(void);
	void drawStones(void);
	void drawStones2(void);
	/*glClear(GL_COLOR_BUFFER_BIT);
	glFlush();*/

	
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);

	//draw the sky
	glLoadIdentity();
	glTranslatef(0.0f, 0.45f, -5.0f);
	drawRectangle(3.0);


	// background landscape
	glLoadIdentity();
	glTranslatef(0.0, 0.0f, -2.0f);
	drawMountains();


	//draw the land
	glLoadIdentity();
	glTranslatef(0.0f, -0.8f, -5.0f);
	drawRectangle(1.0);
	
	// background rocks
	glLoadIdentity();
	glTranslatef(-(xMove+4.0), 0.0f, -8.0f);
	drawStones();
	glLoadIdentity();
	glTranslatef(-xMove + 5.5, 0.2f, -8.0f);
	drawStones();
	glLoadIdentity();
	glTranslatef(-xMove + 6.0, 0.2f, -7.0f);
	drawStones2();

	//draw the road
	glLoadIdentity();
	glTranslatef(0.0f, -0.35f, -5.0f);
	drawRectangle(5.0);

	//=====================Bicycle===================
	// bicycle
	glLoadIdentity();
	
	//back wheel
	glTranslatef(xMove + gfDistanceBetweenWheels, 0.0f, -5.0f);
	glRotatef(angleWheels, 0.0f, 0.0f, 1.0f);
	drawCircle(0.45f);


	//front wheel
	glLoadIdentity();
	glTranslatef(xMove, 0.0f, -5.0f);
	glRotatef(angleWheels, 0.0f, 0.0f, 1.0f);
	drawCircle(0.45f);
	

	//traingle - back and seat
	glLoadIdentity();
	glTranslatef(xMove + 1.5f , 0.0f, -5.0f);
	drawTriangle(1.0);
	
	//traingle - front
	glLoadIdentity();
	glTranslatef(xMove + 1.5f, 0.0f, -5.0f);
	drawTriangle(3.0f);

	// handle
	glLoadIdentity();
	glTranslatef(xMove, 0.0f, -5.0f);
	drawHandle();
	
	glLoadIdentity();
	glTranslatef(-xMove, -0.7f, -5.0f);
	drawStones();
	
	//====================================================================

	SwapBuffers(ghdc);
	
}
// update rotation angles
void update(void) {

	angleWheels += 0.25f;
	if (angleWheels >= 360) {
		angleWheels = 0;
	}

	xMove -= 0.005f;
	if (xMove <= -5.5f) {
		xMove = 4.5f;
	}

}


//draw stones
void drawStones2(void) {

	glBegin(GL_TRIANGLE_FAN);
	//glColor3f(1.0 / 255.0, 10.0 / 255.0, 20.0 / 255.0);
	for (xPara = -0.8; xPara <= 1.2; xPara += 0.35f) {
		if (xPara <= -0.1) {
			glColor3f(25.0 / 255.0, 31.0 / 255.0, 31.0 / 255.0);
		}
		else if (xPara <= 0.5) {
			glColor3f(61.0 / 255.0, 68.0 / 255.0, 68.0 / 255.0);
		}
		else {
			glColor3f(75.0 / 255.0, 85.0 / 255.0, 85.0 / 255.0);
		}
		
		yPara = -((xPara)*(xPara));
		glVertex3f(xPara, yPara, 0.0);
		//glVertex3f(xPara, 0.0, 0.0);
	}
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	//glColor3f(1.0 / 255.0, 10.0 / 255.0, 20.0 / 255.0);
	for (xPara = -1.4; xPara <= 0.45; xPara += 0.25f) {
		if (xPara <= 0.0) {
			glColor3f(29.0 / 255.0, 31.0 / 255.0, 31.0 / 255.0);
		}
		else  {
			glColor3f(50.0 / 255.0, 51.0 / 255.0, 51.0 / 255.0);
		}
		
		yPara = -((xPara + 0.5)*(xPara + 0.5) + 0.3);
		glVertex3f(xPara, yPara, 0.0);
		//glVertex3f(xPara, 0.0, 0.0);
	}
	glEnd();

}

//draw stones
void drawStones(void) {
	
	glBegin(GL_TRIANGLE_FAN);
	//glColor3f(1.0 / 255.0, 10.0 / 255.0, 20.0 / 255.0);
	for (xPara = -0.8; xPara <= 1.2; xPara += 0.35f) {
		if (xPara <= -0.3) {
			glColor3f(1.0 / 255.0, 10.0 / 255.0, 20.0 / 255.0);
		}
		else if (xPara <= 0.8) {
			glColor3f(87.0 / 255.0, 102.0 / 255.0, 102.0 / 255.0);
			//glColor3f(80.0 / 255.0, 80.0 / 255.0, 80.0 / 255.0);
		} 
		else{
			glColor3f(102.0 / 255.0, 104.0 / 255.0, 104.0 / 255.0);
		}
		yPara = -((xPara)*(xPara));
		glVertex3f(xPara, yPara, 0.0);
		//glVertex3f(xPara, 0.0, 0.0);
	}
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	//glColor3f(1.0 / 255.0, 10.0 / 255.0, 20.0 / 255.0);
	for (xPara = -1.4; xPara <= 0.45; xPara += 0.25f) {
		if (xPara <= -0.5) {
			glColor3f(0.0 / 255.0, 0.0 / 255.0, 0.0 / 255.0);
		}
		else if (xPara <= 0.0) {
			glColor3f(5.0 / 255.0, 24.0 / 255.0, 25.0 / 255.0);
		}
		else {
			glColor3f(26.0 / 255.0, 80.0 / 255.0, 90.0 / 255.0);
		}
		yPara = -((xPara+ 0.5)*(xPara + 0.5) +0.3) ;
		glVertex3f(xPara, yPara, 0.0);
		//glVertex3f(xPara, 0.0, 0.0);
	}
	glEnd();

}



//draw moutains
void drawMountains(void) {
	
		glBegin(GL_TRIANGLE_FAN);
	//glColor3f(20.0 / 255.0, 43.0 / 255.0, 2.0 / 255.0);
		//glColor3f(62.0 / 255.0, 94.0 / 255.0, 6.0 / 255.0);
	for (xPara = -2.0; xPara <= 3.0; xPara += 0.03f) {
		if (xPara <= 0.0) {
			glColor3f(40.0 / 255.0, 61.0 / 255.0, 3.0 / 255.0);
		}
		else if (xPara <= 1.0) {
			glColor3f(62.0 / 255.0, 94.0 / 255.0, 6.0 / 255.0);
		}
		yPara = -((xPara - 1.0)*(xPara - 1.0)*0.35) + 0.35 ;
		glVertex3f(xPara, yPara, 0.0);
		//glVertex3f(xPara, 0.0, 0.0);
	}

	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);
	//glColor3f(112.0 / 255.0, 148.0 / 255.0, 14.0 / 255.0);
	glColor3f(13.0 / 255.0, 90.0 / 255.0, 4.0 / 255.0);
	for (xPara = -4.0; xPara <= 1.5; xPara += 0.03f) {
		
		yPara = -((xPara + 0.8)*(xPara + 0.8)*0.2) + 0.3;
		glVertex3f(xPara, yPara, 0.0);
		//glVertex3f(xPara, 0.0, 0.0);
	}
	glEnd();
	
	glBegin(GL_TRIANGLE_FAN);
	//glColor3f(11.0 / 255.0, 114.0 / 255.0, 18.0 / 255.0);
	glColor3f(112.0 / 255.0, 148.0 / 255.0, 14.0 / 255.0);
	
	for (xPara = -2.0; xPara <= 3.0; xPara += 0.03f) {
		yPara = -((xPara - 0.5)*(xPara - 0.5)*0.2) + 0.15;
		glVertex3f(xPara, yPara, 0.0);
		//glVertex3f(xPara, 0.0, 0.0);
	}	
	
	glEnd();





}



//draws handle
void drawHandle(void) {
	glLineWidth(8.0f);
	glBegin(GL_LINE_LOOP);
	glColor3f(0.5, 1.0, 1.0);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f((gfDistanceBetweenWheels / 4.0f)+0.09, 0.75f+0.18, 0.0f);
	glEnd();

	glBegin(GL_LINE_LOOP);
	glVertex3f((gfDistanceBetweenWheels / 4.0f) + 0.09, 0.75f + 0.16, 0.0f);
	glVertex3f((gfDistanceBetweenWheels / 4.0f) + 0.3, 0.75f + 0.16, 0.0f);
	glEnd();
}


// draws triangle
void drawTriangle(float triangleScale) {

	if (triangleScale <= 2.0f) {
		glLineWidth(8.0f);
		glBegin(GL_LINE_LOOP);
		//glColor3f(0.5f, 1.0f, 1.0f);
		//glColor3f(1.0f, 1.0f, 1.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		//glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-(gfDistanceBetweenWheels / 2.0f), -0.06f, 0.0f);
		//glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-(gfDistanceBetweenWheels / 4.0f) , 0.75f, 0.0f);
		glEnd();

		glBegin(GL_TRIANGLES);
		//glColor3f(1.0f, 1.0f, 1.0f);
		//glColor3f(0.0f, 1.0f, 0.0f);
		glColor3f(0.5f, 1.0f, 1.0f);
		glVertex3f(-((gfDistanceBetweenWheels / 4.0f)) + 0.05f, 0.74f, 0.0f);
		glVertex3f(-((gfDistanceBetweenWheels / 4.0f)) + 0.35f, 0.74f, 0.0f);
		glVertex3f(-((gfDistanceBetweenWheels / 4.0f)) + 0.35f, 0.86f, 0.0f);
		glEnd();
	}
	else {
		glBegin(GL_LINE_LOOP);
		//glColor3f(1.0f, 0.0f, 0.0f);
		glColor3f(0.0f, 0.0f, 0.0f);
		glVertex3f(-(gfDistanceBetweenWheels / 2.0f), -0.06f, 0.0f);
		//glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-(gfDistanceBetweenWheels / 4.0f), 0.75f, 0.0f);
		glVertex3f(-(3*(gfDistanceBetweenWheels / 4.0f)), 0.75f, 0.0f);
		glEnd();
	}
	

}

// draws Rectngle
void drawRectangle(float rectangleScale) {

	if (rectangleScale <= 2.0) { //land
		glBegin(GL_QUADS);
		//glColor3f(111.0f/255.0, 72.0/255.0, 4.0/255.0);
		glColor3f(23.0f / 255.0, 9.0 / 255.0, 1.0 / 255.0);
		glVertex3f(-4.0, 0.0, 0.0f);
		//glColor3f(43.0f / 255.0, 20.0 / 255.0, 3.0 / 255.0);
		glColor3f(17.0f / 255.0, 10.0 / 255.0, 3.0 / 255.0);
		glVertex3f(-4.0, -1.5, 0.0f);
		glColor3f(131.0f / 255.0, 75.0 / 255.0, 3.0 / 255.0);
		glVertex3f(4.0, -1.5, 0.0f);
		glColor3f(18.0f / 255.0, 10.0 / 255.0, 4.0 / 255.0);
		glVertex3f(4.0, 0.0, 0.0);
		glEnd();
	}
	else if (rectangleScale <=4.0){ //sky
		glBegin(GL_QUADS);
		//glColor3f(153.0/255.0, 239.0/255.0, 249.0/255.0);
		glColor3f(250.0 / 255.0, 250.0 / 255.0, 250.0 / 255.0);
		glVertex3f(-4.0, 2.0, 0.0f);
		glColor3f(2.0 / 255.0, 13.0 / 255.0, 68.0 / 255.0);
		glVertex3f(-4.0, -0.2, 0.0f);
		glColor3f(1.0 / 255.0, 8.0 / 255.0, 38.0 / 255.0);
		glVertex3f(4.0, -0.2, 0.0f);
		//glColor3f(122.0 / 255.0, 151.0 / 255.0, 229.0 / 255.0);
		glColor3f(201.0 / 255.0, 242.0 / 255.0, 243.0 / 255.0);
		glVertex3f(4.0, 2.0, 0.0);

		glEnd();
	}
	else {

		glBegin(GL_QUADS); // road
		//glColor3f(153.0/255.0, 239.0/255.0, 249.0/255.0);
		glColor3f(80.0 / 255.0, 85.0 / 255.0, 90.0 / 255.0);
		glVertex3f(-4.0, 0.0, 0.0f);
		glColor3f(31.0 / 255.0, 31.0 / 255.0, 31.0 / 255.0);
		glVertex3f(-4.0, -0.5, 0.0f);
		glColor3f(121.0 / 255.0, 134.0 / 255.0, 144.0 / 255.0);
		glVertex3f(4.0, -0.5, 0.0f);
		glColor3f(82.0 / 255.0, 90.0 / 255.0, 95.0 / 255.0);
		glVertex3f(4.0, 0.0, 0.0);

		glEnd();

	}
	

}


void drawCircle(float circleScale) {
	float angle;
	float angleSin;
	glPointSize(8.0f);
	glBegin(GL_POINTS);


	for (int i = 0; i < 1000; i++) {
		angle = 2 * PI * i / 1000.0f;
		if (i < 150) {
			//glColor3f(0.5f, 1.0f, 1.0f);
			glColor3f(249.0/255.0, 10.0/255.0, 34.0/255.0);
		}
		else if (i < 300) {
			//glColor3f(4.0f, 4.0f, 0.0f);
			glColor3f(0.0f, 0.0f, 0.0f);
		}
		else if (i < 450) {
			//glColor3f(4.0f, 4.0f, 0.0f);
			glColor3f(249.0 / 255.0, 10.0 / 255.0, 34.0 / 255.0);
		}
		else if (i < 600) {
			//glColor3f(0.5f, 1.0f, 1.0f);
			glColor3f(0.0f, 0.0f, 0.0f);
		}
		else if (i < 750) {
			//glColor3f(0.5f, 1.0f, 1.0f);
			glColor3f(249.0 / 255.0, 10.0 / 255.0, 34.0 / 255.0);
		}
		else {
			//glColor3f(0.5f, 1.0f, 1.0f);
			glColor3f(0.0f, 0.0f, 0.0f);
		}
		
		glVertex2f((cos(angle) * circleScale), (sin(angle) * circleScale));
	}
	glEnd();

}

// draw graph paper

void drawGraphPaper(void) {

	float xIndex = 0.0f;
	float yIndex = 0.0f;
	glLineWidth(3.0f);
	//start drawing graph
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);
	glEnd();

	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(0.0f, 0.0f, 1.0f);
	for (xIndex = -0.95f; xIndex < 1.0f; (xIndex += 0.05f))
	{
		glVertex3f(xIndex, 1.0f, 0.0f);
		glVertex3f(xIndex, -1.0f, 0.0f);
	}


	for (yIndex = -0.95f; yIndex < 1.0f; (yIndex += 0.05f))
	{
		glVertex3f(1.0f, yIndex, 0.0f);
		glVertex3f(-1.0f, yIndex, 0.0f);
	}
	glEnd();
	// end of graph


}



void resize(int width, int height) {
	if (height == 0) {
		height = 1;
	}
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION); // change matrix mode to projection
	glLoadIdentity();
	//glFrustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 10.0f);
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
	/* Queries :
	1. How can we see the object with FOV angle zero degrees??
	2. Why glTranslate doesn't work when FOV is zero?
	and why it works when FOV is 45?
	*/
	//gluPerspective(0.0f, (GLfloat)width / (GLfloat)height, 1.0f, 100.0f);
}

void uninitialize(void) {

	if (gbFullScreen == true) {
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER |
			SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(ghrc);
	ghrc = NULL;

	DestroyWindow(ghwnd);
}